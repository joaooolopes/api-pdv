package br.com.joaolopes.api.pdv.controllers.utils.paging;

import java.util.List;

public class PageResponse{

    public PageResponse(Page _metadata, List<?> records) {
        this._metadata = _metadata;
        this.records = records;
    }

    private Page _metadata;
    private List<?> records;

    public Page get_metadata() {
        return _metadata;
    }

    public void set_metadata(Page _metadata) {
        this._metadata = _metadata;
    }

    public List<?> getRecords() {
        return records;
    }

    public void setRecords(List<?> records) {
        this.records = records;
    }
}
