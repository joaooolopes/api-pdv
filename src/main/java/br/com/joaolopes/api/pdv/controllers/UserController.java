package br.com.joaolopes.api.pdv.controllers;

import br.com.joaolopes.api.pdv.controllers.utils.Controller;
import br.com.joaolopes.api.pdv.controllers.utils.paging.PageRequest;
import br.com.joaolopes.api.pdv.dao.UserDAO;
import br.com.joaolopes.api.pdv.objects.user.User;
import br.com.joaolopes.api.pdv.objects.user.UserCreateDTO;
import br.com.joaolopes.api.pdv.objects.user.UserUpdateDTO;
import org.codehaus.plexus.util.ExceptionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/users")
public class UserController extends Controller {

    private final UserDAO userDAO;
    private final ModelMapper modelMapper;

    public UserController(UserDAO userDAO, ModelMapper modelMapper) {
        this.userDAO = userDAO;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    private ResponseEntity<?> findAll(@Valid PageRequest pageRequest){
        if(pageRequest.isNull()){
            return ResponseEntity.ok(userDAO.findAll());
        } else {
            return ResponseEntity.ok(super.findPage(pageRequest, userDAO, this));
        }
    }


    @GetMapping("/{id}")
    private User findById(@PathVariable long id){
        return (User) findById(id, userDAO);
    }

    @PostMapping
    private User createUser(@Valid @RequestBody UserCreateDTO userDTO){
        try {
            User user = modelMapper.map(userDTO, User.class);
//            user.setPassword(hashPassword(user.getPassword()));
            userDAO.save(user);
            return user;
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
                    ExceptionUtils.getRootCause(e).getMessage(), e);
        }
    }

    @PutMapping
    private User updateUser(@Valid @RequestBody UserUpdateDTO userDTO){
        try {
            User user = userDAO.findById(userDTO.getId());
            user.setName(userDTO.getName());
            user.setEmail(userDTO.getEmail());
            userDAO.update(user);
            return user;
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
                    ExceptionUtils.getRootCause(e).getMessage(), e);
        }
    }
}
