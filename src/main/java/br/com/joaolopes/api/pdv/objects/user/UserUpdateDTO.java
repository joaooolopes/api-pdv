package br.com.joaolopes.api.pdv.objects.user;

import javax.validation.constraints.*;

public class UserUpdateDTO {

    @NotNull(message = "ID can't be null")
    @Positive(message = "ID can't be negative number")
    private long id;

    @NotBlank(message = "Name can't be blank")
    private String name;

    @NotBlank(message = "E-mail can't be blank")
    @Email(message = "E-mail not valid")
    private String email;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
