package br.com.joaolopes.api.pdv.dao;

import br.com.joaolopes.api.pdv.objects.sale.Sale;
import org.springframework.stereotype.Repository;

@Repository
public class SaleDAO extends HibernateDAO<Sale> {

    public SaleDAO() {
        settClass(Sale.class);
    }
}
