package br.com.joaolopes.api.pdv.dao;

import br.com.joaolopes.api.pdv.controllers.utils.paging.PageRequest;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Transactional
public abstract class HibernateDAO<T extends Serializable> {
    private Class<T> tClass;

    @Autowired
    private SessionFactory sessionFactory;

    protected void settClass(Class<T> tClass){
        this.tClass = tClass;
    }

    protected final Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    public String getClassName(){
        return tClass.getSimpleName();
    }

    public T findById(long id){
        return getSession().get(tClass, id);
    }

    public List<T> findAll(){
        return getSession().createQuery("From " + tClass.getName()).list();
    }

    public void save(T entity){
        getSession().save(entity);
    }

    public void update(T entity){
        getSession().update(entity);
    }

    public long countRows(){
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
        Root<T> from = criteriaQuery.from(tClass);
        criteriaQuery.select(builder.count(from));
        System.out.println(Arrays.toString(tClass.getDeclaredFields()));
        try {
            tClass.getDeclaredField("disabledAt");
            criteriaQuery.where(builder.isNull(from.get("disabledAt")));
        } catch (NoSuchFieldException e) {
            System.err.println(" (" + tClass.getName() + ") Don't use disabled_at on count rows -- " + e.getMessage());
        }
        return getSession().createQuery(criteriaQuery).getSingleResult();
    }

    public List<T> findPage(PageRequest pageRequest){
        int offset = pageRequest.getPage() * pageRequest.getSize();

        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = builder.createQuery(tClass);
        Root<T> from = criteriaQuery.from(tClass);
        criteriaQuery.select(from);

        return getSession()
                .createQuery(criteriaQuery)
                .setFirstResult(offset)
                .setMaxResults(pageRequest.getSize())
                .getResultList();

    }
}
