package br.com.joaolopes.api.pdv.configurations;

import org.flywaydb.core.Flyway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;

@Configuration
public class FlywayConfig {

    private final Environment env;

    public FlywayConfig(Environment env) {
        this.env = env;
    }

    @Bean
    public Flyway flyWay(){
        return Flyway.configure().dataSource(
                env.getProperty("db.url"),
                env.getProperty("db.username"),
                env.getProperty("db.password")
        ).load();
    }

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent e){
        Flyway flyway = e.getApplicationContext().getBean(Flyway.class);
        flyway.migrate();
    }
}
