package br.com.joaolopes.api.pdv.dao;

import br.com.joaolopes.api.pdv.objects.payment.Payment;
import org.springframework.stereotype.Repository;

@Repository
public class PaymentDAO extends HibernateDAO<Payment> {

    public PaymentDAO() {
        settClass(Payment.class);
    }
}
