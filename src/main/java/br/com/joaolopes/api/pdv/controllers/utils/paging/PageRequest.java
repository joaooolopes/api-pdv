package br.com.joaolopes.api.pdv.controllers.utils.paging;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

public class PageRequest {
    @PositiveOrZero(message = "Page must be zero or a positive number")
    private Integer page;

    @Positive(message = "Size must be a positive number")
    private Integer size;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public boolean isNull(){
        return page == null || size == null;
    }

    @Override
    public String toString() {
        return "Page: " + page + "\n" +
                "Size: " + size;
    }
}
