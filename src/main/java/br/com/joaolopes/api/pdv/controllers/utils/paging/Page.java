package br.com.joaolopes.api.pdv.controllers.utils.paging;

import org.springframework.hateoas.RepresentationModel;

public class Page extends RepresentationModel<Page> {

    private long page;
    private long size;
    private long pageCount;
    private long totalCount;

    public long getPage() {
        return page;
    }

    public void setPage(long page) {
        this.page = page;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getPageCount() {
        return pageCount;
    }

    public void setPageCount(long pageCount) {
        this.pageCount = pageCount;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }
}
