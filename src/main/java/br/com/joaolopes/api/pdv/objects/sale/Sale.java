package br.com.joaolopes.api.pdv.objects.sale;

import br.com.joaolopes.api.pdv.objects.customer.Customer;
import br.com.joaolopes.api.pdv.objects.user.User;
import com.sun.istack.NotNull;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;

public class Sale implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @NotNull
    private User user;

    private Customer customer;

    @NotNull
    private double value;

    @NotNull
    private double discount;

    @NotNull
    private double change;

    @Column(name = "created_at")
    private Timestamp createdAt;

    @Column(name = "disabled_at")
    private Timestamp disabledAt;

    private User userDisabled;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getDisabledAt() {
        return disabledAt;
    }

    public void setDisabledAt(Timestamp disabledAt) {
        this.disabledAt = disabledAt;
    }

    public User getUserDisabled() {
        return userDisabled;
    }

    public void setUserDisabled(User userDisabled) {
        this.userDisabled = userDisabled;
    }
}
