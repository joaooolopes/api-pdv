package br.com.joaolopes.api.pdv.dao;

import br.com.joaolopes.api.pdv.objects.cash.Cash;
import org.springframework.stereotype.Repository;

@Repository
public class CashDAO extends HibernateDAO<Cash> {

    public CashDAO() {
        settClass(Cash.class);
    }
}
