package br.com.joaolopes.api.pdv.dao;

import br.com.joaolopes.api.pdv.objects.user.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class UserDAO extends HibernateDAO<User> {

    private final BCryptPasswordEncoder passwordEncoder;

    public UserDAO(BCryptPasswordEncoder passwordEncoder) {
        settClass(User.class);
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void save(User entity) {
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        super.save(entity);
    }

    public User findByEmail(String email){
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = builder.createQuery(User.class);
        Root<User> from = criteriaQuery.from(User.class);
        criteriaQuery.select(from);
        criteriaQuery.where(builder.equal(from.get("email"), email));
        return getSession().createQuery(criteriaQuery).getSingleResult();
    }
}
