package br.com.joaolopes.api.pdv.objects.user;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.Date;

public class UserCreateDTO {

    @NotBlank(message = "Name can't be blank")
    private String name;

    @NotBlank(message = "E-mail can't be blank")
    @Email(message = "E-mail not valid")
    private String email;

    @NotBlank(message = "Password can't be blank")
    @Size(min = 6, message = "Password too short. Min:6")
    private String password;

    @JsonIgnore
    private Timestamp createdAt = new Timestamp(new Date().getTime());

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Name: " + name +
                "\nEmail: " + email +
                "\nPassword: " + password;
    }
}
