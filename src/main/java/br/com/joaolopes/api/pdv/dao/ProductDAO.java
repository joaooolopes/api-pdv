package br.com.joaolopes.api.pdv.dao;

import br.com.joaolopes.api.pdv.objects.product.Product;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDAO extends HibernateDAO<Product> {

    public ProductDAO() {
        settClass(Product.class);
    }
}
