package br.com.joaolopes.api.pdv.security;

public class SecurityConstants {
    public static final String SECRET = "KeyToCreateJWTs";
//    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final long EXPIRATION_TIME = 1_200_000; // 10 min
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users";
}
