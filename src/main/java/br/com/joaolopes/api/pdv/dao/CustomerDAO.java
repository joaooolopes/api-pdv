package br.com.joaolopes.api.pdv.dao;

import br.com.joaolopes.api.pdv.objects.customer.Customer;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerDAO extends HibernateDAO<Customer> {

    public CustomerDAO() {
        settClass(Customer.class);
    }
}
