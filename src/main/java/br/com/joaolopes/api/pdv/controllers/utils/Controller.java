package br.com.joaolopes.api.pdv.controllers.utils;

import br.com.joaolopes.api.pdv.controllers.utils.paging.Page;
import br.com.joaolopes.api.pdv.controllers.utils.paging.PageRequest;
import br.com.joaolopes.api.pdv.controllers.utils.paging.PageResponse;
import br.com.joaolopes.api.pdv.dao.HibernateDAO;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Controller {

    protected Object findById(long id, HibernateDAO<?> dao){
        Object object = dao.findById(id);
        if(object != null){
            return object;
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    dao.getClassName() + " ID:" + id + " doesn't exist" );
    }

    protected PageResponse findPage(PageRequest pageRequest, HibernateDAO<?> dao,
                                    Controller controller){
        long pageNumber = pageRequest.getPage();
        long sizeNumber = pageRequest.getSize();
        long totalNumber = dao.countRows();

        List<?> records = dao.findPage(pageRequest);
        if(records.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "This page from " + dao.getClassName() + " is empty!" );
        }

        Page page = new Page();
        page.setPage(pageNumber);
        page.setSize(sizeNumber);
        page.setPageCount(records.size());
        page.setTotalCount(totalNumber);

        long prev = pageNumber > 0 ? pageNumber - 1 : 0;
        long last = (totalNumber / sizeNumber) - 1;
        last = last < 0 ? 0 : last;
        long next = pageNumber < last ? pageNumber + 1 : pageNumber;


        Link defaultLink = WebMvcLinkBuilder.linkTo(controller.getClass()).withSelfRel();
        defaultLink = Link.of(defaultLink.getHref() + "{?page}{?size}");

        page.add(defaultLink.expand(0, sizeNumber).withRel(IanaLinkRelations.FIRST));
        page.add(defaultLink.expand(prev, sizeNumber).withRel(IanaLinkRelations.PREV));
        page.add(defaultLink.expand(pageRequest.getPage(), sizeNumber).withSelfRel());
        page.add(defaultLink.expand(next, sizeNumber).withRel(IanaLinkRelations.NEXT));
        page.add(defaultLink.expand(last, sizeNumber).withRel(IanaLinkRelations.LAST));

        return new PageResponse(page, records);
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        return handleError(ex.getBindingResult());
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(BindException.class)
    public Map<String, String> handleValidationExceptions(BindException ex) {
        return handleError(ex.getBindingResult());
    }

    private Map<String, String> handleError(BindingResult bindingResult) {
        Map<String, String> errors = new HashMap<>();
        bindingResult.getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
