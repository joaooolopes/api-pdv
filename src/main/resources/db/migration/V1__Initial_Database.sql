create table if not exists users
(
  id          serial                  not null
    constraint users_pk
      primary key,
  name        varchar                 not null,
  email       varchar                 not null,
  password    varchar                 not null,
  created_at  timestamp default now() not null,
  disabled_at timestamp
);

comment on table users is 'Usuarios do Sistema';

comment on column users.name is 'Nome do Usuario';

comment on column users.email is 'Email do Usuario';

comment on column users.password is 'Senha do Usuario';

comment on column users.created_at is 'Data de Criacao do Usuario';

comment on column users.disabled_at is 'Data de Desativacao do Usuario';

alter table users
  owner to pdv_api;

create unique index if not exists users_email_uindex
  on users (email);

create unique index if not exists users_id_uindex
  on users (id);

create table if not exists customers
(
  id          serial                  not null
    constraint clientes_pk
      primary key,
  name        varchar                 not null,
  document    varchar,
  phone       varchar,
  created_at  timestamp default now() not null,
  disabled_at timestamp
);

comment on table customers is 'Clientes do PDV';

comment on column customers.name is 'Nome do Cliente';

comment on column customers.document is 'Documento do Cliente';

comment on column customers.phone is 'Telefone do Cliente';

comment on column customers.created_at is 'Data de criacao do Cliente';

comment on column customers.disabled_at is 'Data de desativacao do cliente';

alter table customers
  owner to pdv_api;

create unique index if not exists clientes_document_uindex
  on customers (document);

create unique index if not exists clientes_id_uindex
  on customers (id);

create table if not exists products
(
  id          serial                  not null
    constraint products_pk
      primary key,
  name        varchar                 not null,
  price       numeric(10, 2)          not null,
  quantity    integer   default 0     not null,
  created_at  timestamp default now() not null,
  disabled_at timestamp
);

comment on table products is 'Produtos do Sistema';

comment on column products.name is 'Nome do Produto';

comment on column products.price is 'Preço do Produto';

comment on column products.quantity is 'Quantidade do Produto no Estoque';

comment on column products.created_at is 'Data de criacao do Produto';

comment on column products.disabled_at is 'Data de Desativacao do Produto';

alter table products
  owner to pdv_api;

create unique index if not exists products_id_uindex
  on products (id);

create table if not exists cashes
(
  id            serial                  not null
    constraint cashes_pk
      primary key,
  user_id       integer                 not null
    constraint cashes_users_id_fk
      references users,
  initial_value numeric(10, 2)          not null,
  created_at    timestamp default now() not null,
  disabled_at   timestamp
);

comment on column cashes.user_id is 'Usuario responsavel pelo caixa';

comment on column cashes.initial_value is 'Valor inicial do caixa';

comment on column cashes.created_at is 'Data de abertura do caixa';

comment on column cashes.disabled_at is 'Data de fachamento do caixa';

alter table cashes
  owner to pdv_api;

create unique index if not exists cashes_id_uindex
  on cashes (id);

create table if not exists sales
(
  id            serial                  not null
    constraint sales_pk
      primary key,
  user_id       integer                 not null
    constraint sales_users_id_fk
      references users,
  customer_id   integer
    constraint sales_customers_id_fk
      references customers,
  value         numeric(10, 2)          not null,
  discount      numeric(10, 2)          not null,
  change        numeric(10, 2)          not null,
  created_at    timestamp default now() not null,
  disabled_at   timestamp,
  user_disabled integer
    constraint sales_users_id_fk_2
      references users
);

comment on table sales is 'Vendas do Sistema';

comment on column sales.user_id is 'Usuario que realizou a venda';

comment on column sales.customer_id is 'Cliente que executou a compra';

comment on column sales.value is 'Valor total da venda';

comment on column sales.discount is 'Desconto aplicado na venda';

comment on column sales.change is 'Troco da Venda';

comment on column sales.created_at is 'Data que a venda foi realizada';

comment on column sales.disabled_at is 'Data de Desativacao da Venda';

comment on column sales.user_disabled is 'Usuario que desativou a Venda';

alter table sales
  owner to pdv_api;

create unique index if not exists sales_id_uindex
  on sales (id);

create table if not exists payment
(
  id      serial         not null
    constraint payment_pk
      primary key,
  sale_id integer        not null
    constraint payment_sales_id_fk
      references sales,
  name    varchar        not null,
  value   numeric(10, 2) not null
);

comment on table payment is 'Pagamentos do sistema';

comment on column payment.sale_id is 'Venda que o Pagamento Pertence';

comment on column payment.name is 'Metodo de Pagamento';

comment on column payment.value is 'Valor do Pagamento';

alter table payment
  owner to pdv_api;

create unique index if not exists payment_id_uindex
  on payment (id);

